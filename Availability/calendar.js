var replacing = [];

const submit = async () => {
    var dateoverrides = JSON.parse(dbdata.dateOverrides);
    if(parseInt(dateoverwrites.length)>0){
        if(parseInt(replacing.length) == 0){
            for(var i = 0; i < dateoverwrites.length; i++){
                dateoverrides.push({
                    dates: dateoverwrites[i].dates || [],
                    slots: dateoverwrites[i].slots || [],
                    overwriteId: dateoverwrites[i].overwriteId
                })
            }
        } else{
            for(var i = 0; i < dateoverwrites.length; i++){
                var replaced=false;
                for(var r = 0; r < replacing.length; r++){
                    if(dateoverwrites[i].overwriteId == replacing[r]){
                        replaced = true;
                    }
                }
                if(replaced){
                    for(var rd = 0; rd < dateoverrides.length; rd++){
                        if(dateoverrides[rd].overwriteId == dateoverwrites[i].overwriteId){
                            dateoverrides[rd].slots = dateoverwrites[i].slots;
                        }
                    }
                }else{
                    dateoverrides.push({
                        dates: dateoverwrites[i].dates || [],
                        slots: dateoverwrites[i].slots || [],
                        overwriteId: dateoverwrites[i].overwriteId
                    })
                }
            }
        }
        $.ajax({
            url: 'updateOverwrites.php',
            type: 'POST',
            data: {sttafId: 1, dateOverrides: dateoverrides},
        })
        .done(function(msg) {
            //console.log("success");
            alert(msg);
        })
        .fail(function() {
            //console.log("error");
        });
        //console.log(dateoverrides)
    } else{
        alert("No date selected to overwrite")
    }
}

var dateoverwrites = [

];
var weeks = [
    "sunday",
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
    "saturday"
];
$(document).ready(function() {
    $('#datepicker').datepicker({
        multidate: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',
        clearBtn: true,
        startDate: '-0m'
    });
    $('#datepicker').on('changeDate', async function(evt) {
        dateoverwrites = await [];
        const d = new Date(evt.date);
        for(var i = 0; i < evt.dates.length; i++){
            await dateoverwrites.push({dates: [evt.dates[i]], overwriteId: i, slots: []});
        }
        $(".time").html("");
        var dateIndex = 0;
        var overrides = JSON.parse(dbdata.dateOverrides);
                        replacing = await [];
        for(var a = 0;  a < dateoverwrites.length; a++){
              if(dateoverwrites[a].dates[0].toString() == evt.date.toString()){
                    dateoverwrites[a].slots = await [];
                    for(var i = 0; i < overrides.length; i++){
                        for(var di = 0; di < overrides[i].dates.length; di++)

                        if(overrides[i].dates[di] == evt.date){
                            if(overrides[i].slots){
                            var dateIndex = 1;
                                for(var s = 0; s < overrides[i].slots.length; s++){
                                    var slot = overrides[i].slots[s];
                                    dateoverwrites[a].slots.push({
                                        key: s + 1,
                                        startTime: slot.startTime,
                                        endTime: slot.endTime,
                                        duration: 30
                                    });
                                    $(".time").append(`
                                    <div class="slot">
                                        <input
                                        class="startTime"
                                        type="time"
                                        id="${s+1}"
                                        onchange="change(this)"
                    
                                        value="${slot.startTime}"
                                        />
                                        - <input
                                        class="endTime"
                                        type="time"
                                        id="${s+1}"
                                        onchange="change(this)"
                    
                                        value="${slot.endTime}"
                                        />
                                        <svg width="24" height="24" onclick="deleteSlot(${s+1})" style="cursor: pointer"; xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                                            <path d="M19 24h-14c-1.104 0-2-.896-2-2v-16h18v16c0 1.104-.896 2-2 2m-9-14c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6 0c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6-5h-20v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2zm-12-2h4v-1h-4v1z" fill="#f00" />
                                        </svg>
                                    </div><br/>
                                    `);
                                }
                            }
                        }
                    }
                //console.log(dateIndex)
                if(!dateIndex){
                    if (dbdata[weeks[d.getDay()]] && evt.dates.length <= 1) {
                        var slot_ = dbdata[weeks[d.getDay()]];
                        var slot = JSON.parse(slot_);
                        $(".time").html("");
                        dateoverwrites[a].slots = [];
                        for (var i = 0; i < slot.start_time.length; i++) {
                            dateoverwrites[a].slots.push({
                                key: i + 1,
                                startTime: slot.start_time[i],
                                endTime: slot.end_time[i],
                                duration: 30,
                            });
                            $(".time").append(`
                                <div class="slot">
                                    <input
                                    class="startTime"
                                    type="time"
                                    id="${i+1}"
                                    onchange="change(this)"
                
                                    value="${slot.start_time[i]}"
                                    />
                                    - <input
                                    class="endTime"
                                    type="time"
                                    id="${i+1}"
                                    onchange="change(this)"
                
                                    value="${slot.end_time[i]}"
                                    />
                                    <svg width="24" height="24" onclick="deleteSlot(${i+1})" style="cursor: pointer"; xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                                        <path d="M19 24h-14c-1.104 0-2-.896-2-2v-16h18v16c0 1.104-.896 2-2 2m-9-14c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6 0c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6-5h-20v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2zm-12-2h4v-1h-4v1z" fill="#f00" />
                                    </svg>
                                </div><br/>
                                `);
                        }
                    } else {
                        for(var g = 0; g < dateoverwrites.length; g++){
                                dateoverwrites[g].slots = await [];
                                dateoverwrites[g].slots.push({
                                    key: 1,
                                    startTime: "09:00",
                                    endTime: "17:00",
                                    duration: 30
                                });
                        }
                        $(".time").html("");
                        $(".time").append(`
                        <div class="slot">
                            <input
                            class="startTime"
                            type="time"
                            id="1"
                            onchange="change(this)"
                            value="09:00"
                            />
                            - <input
                            class="endTime"
                            type="time"
                            id="1"
                            onchange="change(this)"
                            value="17:00"
                            />
                            <svg width="24" height="24" onclick="deleteSlot(1)" style="cursor: pointer"; xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                                <path d="M19 24h-14c-1.104 0-2-.896-2-2v-16h18v16c0 1.104-.896 2-2 2m-9-14c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6 0c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6-5h-20v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2zm-12-2h4v-1h-4v1z" fill="#f00" />
                            </svg>
                        </div><br/>
                        `);
                    }
                }
            }
        }
        for(var b = 0; b < evt.dates.length; b++){
            if(overrides[b]){
                if(overrides[b].dates[0] == evt.dates[b]){
                    var overrides = await JSON.parse(dbdata.dateOverrides);
                    replacing.push(parseInt(overrides[b].overwriteId));
                }
            }
        }
                if(evt.dates.length){
                    $(".time").append(`<span class="btnAdd" onclick="addSlot()">+ Add Slot</span>`);
                }
    });
    $.ajax({
            url: 'getSchedules.php',
            type: 'POST',
            data: { sttafId: 1 },
        })
        .done(function(data) {
            dbdata = JSON.parse(data);
        })
        .fail(function() {
            //console.log("error");
        });
});
const change = (e) => {
    for(var d = 0; d < dateoverwrites.length; d++){
        for(var i = 0; i < dateoverwrites[d].slots.length; i++){
            if(dateoverwrites[d].slots[i].key == e.attributes.id.nodeValue){
                dateoverwrites[d].slots[i][e.attributes.class.nodeValue] = e.value;
            }
        }
    }
}

const addSlot = async () => {
    for(var i = 0; i < dateoverwrites.length; i++){
        await dateoverwrites[i].slots.push({
            key: dateoverwrites[i].slots.length + 1,
            startTime: "09:00",
            endTime: "17:00",
            duration: 30
        });
    }
    await $(".time").html("");
    for (var i = 0; i < dateoverwrites[0].slots.length; i++) {
        $(".time").append(`
                <div class="slot">
                    <input
                        class="startTime"
                        type="time"
                        id="${dateoverwrites[0].slots[i].key}"
                        onchange="change(this)"
    
                        value="${dateoverwrites[0].slots[i].startTime}"
                    />
                    - <input
                        class="endTime"
                        type="time"
                        id="${dateoverwrites[0].slots[i].key}"
                        onchange="change(this)"
    
                        value="${dateoverwrites[0].slots[i].endTime}"
                    />
                    <svg width="24" height="24" onclick="deleteSlot(${dateoverwrites[0].slots[i].key})" style="cursor: pointer"; xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                        <path d="M19 24h-14c-1.104 0-2-.896-2-2v-16h18v16c0 1.104-.896 2-2 2m-9-14c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6 0c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6-5h-20v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2zm-12-2h4v-1h-4v1z" fill="#f00" />
                    </svg>
                </div><br/>
            `);
    }
    $(".time").append(`<span class="btnAdd" onclick="addSlot()">+ Add Slot</span>`)
}

const deleteSlot = async (keyOfField) => {
    for(var d = 0; d < dateoverwrites.length; d++){
        for (var i = 0; i < dateoverwrites[d].slots.length; i++) {
            if (dateoverwrites[d].slots[i].key == keyOfField) {
                await dateoverwrites[d].slots.splice(i, "1");
            }
        }
    }
    await $(".time").html("");
    for(var d = 0; d < dateoverwrites.length; d++)
    {
        for (var i = 0; i < dateoverwrites[d].slots.length; i++) {
            $(".time").append(`
                <div class="slot">
                    <input
                    class="startTime"
                    type="time"
                    id="${dateoverwrites[d].slots[i].key}"
                    onchange="change(this)"
                    value="${dateoverwrites[d].slots[i].startTime}"
                    />
                    - <input
                    class="endTime"
                    type="time"
                    id="${dateoverwrites[d].slots[i].key}"
                    onchange="change(this)"
                    value="${dateoverwrites[d].slots[i].endTime}"
                    />
                    <svg width="24" height="24" onclick="deleteSlot(${dateoverwrites[d].slots[i].key})" style="cursor: pointer"; xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd">
                        <path d="M19 24h-14c-1.104 0-2-.896-2-2v-16h18v16c0 1.104-.896 2-2 2m-9-14c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6 0c0-.552-.448-1-1-1s-1 .448-1 1v9c0 .552.448 1 1 1s1-.448 1-1v-9zm6-5h-20v-2h6v-1.5c0-.827.673-1.5 1.5-1.5h5c.825 0 1.5.671 1.5 1.5v1.5h6v2zm-12-2h4v-1h-4v1z" fill="#f00" />
                    </svg>
                </div><br/>
            `);
        }
    }
    if(dateoverwrites.length){
        $(".time").append(`<span class="btnAdd" onclick="addSlot()">+ Add Slot</span>`);
    }
}