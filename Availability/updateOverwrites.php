<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "probooking";

// Create connection
$conn = @mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die(json_encode(array('error' => mysqli_connect_error())));
}

$sttafId = $_POST['sttafId'];
$dateOverrides = json_encode($_POST['dateOverrides']);

$sql = "UPDATE staff_schedules SET `dateOverrides` = '$dateOverrides' WHERE `staff_id` = '$sttafId'";
$result = @mysqli_query($conn, $sql);

if ($result){
    echo 'Date Override Added';
} else{
    echo "Cannot Add Date Override";
}

@mysqli_close($conn);
?>