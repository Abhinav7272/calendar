<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "probooking";

// Create connection
$conn = @mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die(json_encode(array('error' => mysqli_connect_error())));
}

$sttafId = $_POST['sttafId'];

$sql = "SELECT * FROM staff_schedules WHERE `staff_id` = $sttafId";
$result = @mysqli_query($conn, $sql);

if (@mysqli_num_rows($result) > 0) {
  // output data of each row
  while($row = @mysqli_fetch_assoc($result)) {
    echo json_encode($row);
  }
} else {
  echo "0 results";
}

@mysqli_close($conn);
?>