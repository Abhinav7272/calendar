<?php

if (isset($_POST['fetchslot']))
{
    $weeks = array(
        "Mon" => json_encode(
            array(
                'openingTime'=>"09:00:00", 
                'closingTime'=> "17:00:00",
                "duration"=> "30"
            )
        ),
        "Wed" => json_encode(
            array(
                'openingTime'=>"08:00:00", 
                'closingTime'=> "17:00:00",
                "duration"=> "35"
            )
        ),
        "Thu" => json_encode(
            array(
                'openingTime'=>"11:00:00", 
                'closingTime'=> "15:00:00",
                "duration"=> "40"
            )
        ),
        "Fri" => json_encode(
            array(
                'openingTime'=>"10:00:00", 
                'closingTime'=> "16:00:00",
                "duration"=> "45"
            )
        )
    );

    $slot = $_POST['fetchslot'];
    $date = new DateTime($slot);
    $availablity = @json_decode($weeks[$date->format('D')]);
    if(isset($availablity)){
        $istTimezone = new DateTimeZone('Asia/Calcutta');
        $openingTime =new DateTime($date->format('D M d Y').' '.$availablity->openingTime,$istTimezone );
        $closingTime=new DateTime($date->format('D M d Y').' '.$availablity->closingTime,$istTimezone );

        getTimeSlot($openingTime, $closingTime, 10, $availablity->duration);
    }else{
        echo json_encode(array("error"=>"No Slots Available"));
    }
}

function getTimeSlot($openingTime, $closingTime, $interval, $duration){
    $timesSlots = array();

    $currentTime = $openingTime;
    $istTimezone = new DateTimeZone('Asia/Calcutta');
    $meetingEndTime = new DateTime($openingTime->format('D M d Y H:i:s'), $istTimezone);
    $meetingEndTime->modify('+'.$duration. 'minutes');
    $closingTime = $closingTime->modify('-'.$duration. 'minutes');
    $interval = $interval;

    while($currentTime <= $closingTime){
        array_push($timesSlots, $currentTime->format("h:i").' - '.$meetingEndTime->format("h:i"));
        $currentTime->modify('+'.$interval. 'minutes');
        $meetingEndTime->modify('+'.$interval. 'minutes');
    }
    echo json_encode($timesSlots);
}

?>

